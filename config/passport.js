/**
 * Passport configuration
 *
 * This is the configuration for your Passport.js setup and where you
 * define the authentication strategies you want your application to employ.
 *
 * I have tested the service with all of the providers listed below - if you
 * come across a provider that for some reason doesn't work, feel free to open
 * an issue on GitHub.
 *
 * Also, authentication scopes can be set through the `scope` property.
 *
 * For more information on the available providers, check out:
 * http://passportjs.org/guide/providers/
 */

module.exports.passport = {
  local: {
    strategy: require('passport-local').Strategy
  },
  facebook: {
    name: 'Facebook',
    protocol: 'oauth2',
    strategy: require('passport-facebook').Strategy,
    options: {
      clientID: '963935006970786',
      clientSecret: '3973e95371ea59c468b908facc490621',
      scope: ['email'] /* email is necessary for login behavior */,
      callbackURL: 'https://www.linkaw.com/auth/facebook/callback'
    }
  },

  google: {
    scope: ['email'],
    name: 'Google',
    protocol: 'oauth2',
    strategy: require('passport-google-oauth').OAuth2Strategy,
    options: {
      clientID: '1085699422997-pfhhgmumm6kbrhl1etifsroq48lisu4r.apps.googleusercontent.com',
      clientSecret: 'VtGOn9bwdIoQzTJnTxKBU2SB',
      callbackURL: 'https://www.linkaw.com/auth/google/callback'
    }
  }
};
