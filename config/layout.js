/**
 * Layout Settings
 *
 * This allows to show some specific part of the aplication to an specific user
 */


module.exports.actionLayout = {

  'initiative': {
    'viewHeader': [
      {
        item: "Users",
        i18nKey: "Users",
        action: "toggleAddCollaboratorBox()",
        image: "fa fa-users",
        href: ""
      },
      {
        item: "Chat",
        i18nKey: "Chat",
        action: "toggleChatBox()",
        image: "fa fa-comments-o",
        href: ""
      },
      {
        item: "Comments",
        i18nKey: "Comments",
        action: "toggleCommentsBox()",
        image: "fa fa-tasks",
        href: ""
      },
      {
        item: "Header",
        i18nKey: "Header",
        action: null,
        image: "fa fa-th-large",
        href: "#headerTitle"
      },
      {
        item: "Articles",
        i18nKey: "Articles",
        action: null,
        image: "fa fa-th-list",
        href: "#articlesTitle"
      },
      {
        item: "LinkedData",
        i18nKey: "LinkedData",
        action: "goToLinkedData()",
        image: "fa fa-database",
        href: ""
      }
    ],
    'editArticles': [
      {
        item: "Chat",
        i18nKey: "Chat",
        action: "toggleChatBox()",
        image: "fa fa-comments-o",
        href: ""
      },
      {
        item: "Comments",
        i18nKey: "Comments",
        action: "toggleCommentsBox()",
        image: "fa fa-tasks",
        href: ""
      },
      {
        item: "SaveArticle",
        i18nKey: "SaveArticle",
        action: "saveArticle()",
        image: "fa fa-save",
        href: ""
      },
      {
        item: "SaveVersion",
        i18nKey: "SaveVersion",
        action: "saveVersion()",
        image: "fa fa-code-fork",
        href: ""
      },
      {
        item: "Initiative",
        i18nKey: "Initiative",
        action: "goToInitiative()",
        image: "fa fa-sitemap",
        href: ""
      },
      {
        item: "LinkedData",
        i18nKey: "LinkedData",
        action: "goToLinkedData()",
        image: "fa fa-database",
        href: ""
      }
    ]
  }
};

module.exports.roleActions = {

  'initiative': {
    'collaborator': [
      {
        item: "Edit",
        i18nKey: "Edit",
        action: null,
        image: "fa fa-search",
        href: "/nada"
      }
    ],
    'author': [
      {
        item: "Edit",
        i18nKey: "Edit",
        action: null,
        image: "fa fa-search",
        href: ""
      },
      {
        item: "Delete",
        i18nKey: "Delete",
        action: null,
        image: "fa fa-plus-square",
        href: "/createeditinitiative"
      }
    ]
  }
};
