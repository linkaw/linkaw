console.log('well configuration works');

var jsonld = require('jsonld').promises;
module.exports.jsonld=jsonld;

console.log('arrancando la iniciativa');

var user = {
  "name": "EL",
  "lastName": "TERCERO",
  "email": "eltercero@gmail.com",
  "identification": "2004",
  "role": "user",
  "image": "http://manu.sporny.org/images/manu.png",
  "name": "Manu Sporny",
  "homepage": "http://manu.sporny.org/"
};

var context = {
  "name": "http://schema.org/name",
  "lastName": {"@id": "http://schema.org/lastName", "@type": "@id"},
  "role": {"@id": "http://schema.org/role", "@type": "@id"}
};

user["@context"] = context;
console.log('userContext', user);


var expanded = jsonld.expand(user)
  .then(function(userExpanded){
    console.log('expanded', JSON.stringify(userExpanded[0], null, 2));
  });

