/**
 * ArticleVersion
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var ArticleVersion = {

  attributes: {
    versionNumber: {
      type: 'integer',
      autoIncrement: true,
      unique: true
    },
    author: {
      model:'user'
    },
    text: {
      type: 'string',
      required: true
    },
    ofArticle:{
      model:'article'
    }
  }

};
module.exports = ArticleVersion;