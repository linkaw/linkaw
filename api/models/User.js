var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    username: {
      type: 'string',
      unique: true },
    name: {
      type: 'string',//validacion en el nombre para solo letras
      required: true
    },
    lastName: {
      type: 'string',//validacion en el nombre para solo letras
      required: true
    },
    email: {
      type: 'email',
      required: true
    },
    photoUrl: {
      type: 'string'
    },
    identification: {
      type: 'string',
      unique: true,
      numeric: true,
      required: false
    },
    role: {
      type: 'string',
      defaultsTo: "user"
    },
    passports: {
      collection: 'Passport',
      via: 'user'
    },
    initiatives: {
      collection: 'initiative',
      via: 'author'
    },
    colaboratorOf: {
      collection: 'initiative',
      via: 'collaborators'
    },
    articles: {
      collection: 'article',
      via: 'author'
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate: function (values, next) {
    //Transform values into Uppercase
    values.name = values.name.toUpperCase();
    console.log(values.name);
    values.lastName = values.lastName.toUpperCase();
    console.log(values.lastName);
    next(null, values);
  },
  beforeCreate: function (user, next) {

    return next();
  }
};

module.exports = User;
