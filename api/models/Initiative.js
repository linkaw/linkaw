/**
 * Initiative
 *
 * @module      :: Initiative
 * @description :: Is a legal Initiative
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var Initiative = {
  schema: true,
  attributes: {  	
    title:{
      type: 'string',
      required: true
    },
    description:{
      type: 'string'
    },
    motivation:{
      type: 'string'
    },
    legalFramework: {
      type: 'string'
    },
    author:{
      model:'user'
    },
    collaborators: {
      collection: 'user',
      via: 'colaboratorOf'
    },
    articles: {
      collection: 'article',
      via: 'ofInitiative'
    },
    userLikesIds: {
      type: 'array',
      defaultsTo:[]
    },
    userDislikesIds: {
      type: 'array',
      defaultsTo:[]
    },
    comments:{
      collection: 'comment',
      via: 'ofInitiative'
    },
    messages:{
      collection: 'message',
      via: 'ofInitiative'
    }
  } 
};
module.exports = Initiative;