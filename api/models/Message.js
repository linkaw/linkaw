/**
 * ChatRoom
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs    :: http://sailsjs.org/#!documentation/models
 */

var Message = {
  attributes: {
    text: {
      type: 'string'
    },
    ofInitiative:{
      model:'initiative'
    },
    ofArticle:{
      model:'article'
    },
    writtenBy:{
      model:'user'
    }
  }
};

module.exports = Message;