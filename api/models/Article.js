/**
 * Article
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var Article = {

  attributes: {
    ofInitiative:{
      type: 'string',
      required: true
    },
    title:{
      type: 'string'
    },
    articleNumber:{
      type: 'integer',
      required: true,
      maxLength:3 
    },
    problem:{
      type: 'string'
    },
    textOfModification: {
      type: 'string'
    },
    currentVersion: {
      type: 'string'
    },
    author: {
      model:'user'
    },
    versions: {
      collection: 'articleversion',
      via: 'ofArticle'
    },
    ofInitiative:{
      model:'initiative'
    },
    comments:{
      collection: 'comment',
      via: 'ofArticle'
    },
    messages:{
      collection: 'message',
      via: 'ofArticle'
    }
  }

};
module.exports = Article;