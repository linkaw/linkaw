/**
 * Comment
 *
 * @module      :: Comment Room
 * @description :: Room to hold the comments over an object
 * @docs    :: http://sailsjs.org/#!documentation/models
 */

var Comment = {
  attributes: {
    text: {
      type: 'string'
    },
    reply: {
      type: 'array'
    },
    status: {
      type: 'string',
      enum: ['opened', 'ignored','solved']
    },
    ofInitiative:{
      model:'initiative'
    },
    ofArticle:{
      model:'article'
    },
    commentedBy:{
      model:'user'
    }
  }
};
module.exports = Comment;