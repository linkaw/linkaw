var Q = require("q");

exports.initiative = {

  // Custom library of useful methods 

  isAuthor: function (initiativeId, userId) {
    var promise = Initiative.findOne()
      .where(
      { id: initiativeId,
        author: userId
      }
    ).then(function (foundInitiative) {
        if (foundInitiative) {
          console.log("iniciative ", initiativeId, "is an author ", userId);
          return true;
        } else {
          console.log("iniciative ", initiativeId, "is not an author ", userId);
          return false;
        }
      }).fail(function (err) {
        console.log("Not Author", err);
        return false;
      });
    return promise;
  },

  isCollaborator: function (initiativeId, userId) {
    var promise = Initiative.findOne(initiativeId)
      .populate("collaborators", {where: {id: userId}})
      .then(function (foundInitiative) {
        if (foundInitiative) {
          console.log("iniciative ", initiativeId, "is a Collaborator", userId);
          return true;
        } else {
          console.log("iniciative ", initiativeId, "is Not a Collaborator", userId);
          return false;
        }
      }).fail(function (err) {
        console.log("Not Collaborator", err);
        return false;
      });
    return promise;
  }

};

