/**
 * InicitiveController
 *
 * @module      :: Controller
 * @description  :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var underscore = require('underscore');
var q = require('q');
var jsonld = require('jsonld').promises;

var context = {
  "legislator": "https://legislator.com/resources/initiative/",
  "foaf": "http://xmlns.com/foaf/0.1/",
  "metalex": "http://www.metalex.eu/metalex/2008-05-02#",
  "dct": "http://purl.org/dc/terms/",
  "opmv": "http://purl.org/net/opmv/ns#",
  "text": "dct:text",
  "title": "dct:title",
  "description": "dct:subject",
  "motivation": "metalex:fragment",
  "legalFramework": "metalex:fragment",
  "author": "metalex:realizedBy",
  "collaboratorsIds": "metalex:realizedBy",
  "userLikesIds": "legislator:userLike",
  "userDislikesIds": "legislator:userDislike",
  "comments": "legislator:comments",
  "homepage": "foaf:homepage",
  "page": "foaf:page",
  "createdAt":"opmv:wasGeneratedAt"
};

var addContext = function (initiative) {
  initiative["@context"] = context;
  initiative["@type"] = 'text';
  initiative["@id"] = 'https://legislator.com/resources/article/' + initiative.id;
  initiative["page"] = 'https://legislator.com/page/article/' + initiative.id;
  initiative["homepage"] = 'https://legislator.com/resources/article/' + initiative.id;
  console.log('user with Context', initiative);
  return initiative;
};
var addContextToList = function (list) {
  console.log("adding context to list", list);
  underscore.each(list, function (item) {
    addContext(item)
  });
  console.log("addedcontext to list", list);
  return list;
};

module.exports = {
  createEdit: function (req, res, next) {

    console.log("CreateEdit");

    var handleError = function (error) {
      console.log(error);
      return console.log(error);
    };

    var publishInitiativeCreation = function (initiative) {
      delete initiative.createdAt;
      delete initiative.updatedAt;
      console.log("Initiative created:", initiative);

      //publish the creation
      Initiative.publishCreate(
        initiative.toJSON()
      );

      //redirect to edition page
      res.redirect(
          "legislator/edit/" + initiative.id
      );
    };

    var createInitiative = function () {
      console.log("creando la iniciativa promesas");
      console.log("CreateInitiative");
      return Initiative.create({
        title: res.__('NewInitiative'),
        author: req.session.user.id
      });
    };

    createInitiative()
      .then(publishInitiativeCreation)
      .fail(function (error) {
        console.log("Error creating the initiative", error);
        return next();
      });

  },
  view: function (req, res) {
    var initiativeId = req.param('initiativeId');
    res.view(
      {
        initiativeId: initiativeId,
        actionBarLayout: sails.config.actionLayout.initiative.viewHeader
      },
      "initiative/view"
    );

  },
  edit: function (req, res) {
    res.view(
      {
        actionBarLayout: sails.config.actionLayout.initiative.editHeader
      },
      "initiative/edit"
    );

  },
  list: function (req, res, next) {
    res.view(
      {
        actionBarLayout: sails.config.actionLayout.initiative.list
      },
      "initiative/list"
    );
  },
  myList: function (req, res, next) {
    res.view(

      {
        actionBarLayout: sails.config.actionLayout.initiative.myList
      },
      'initiative/myList'
    );
  },
  sharedList: function (req, res, next) {
    res.view(

      {
        actionBarLayout: null
      },
      'initiative/sharedList'
    );
  },
  likeInitiative: function (req, res, next) {
    var initiativeId = req.param("initiativeId");
    var userId = req.param("userId");

    Initiative.findOne()
      .where({ id: initiativeId })
      .then(function (foundInitiative, err) {

        if (err) {
          console.log("error searching", err, foundInitiative);
          return next(err);
        }
        if (!foundInitiative.userLikesIds) {
          foundInitiative.userLikesIds = new Array();
        }

        foundInitiative.userLikesIds = underscore.union(foundInitiative.userLikesIds, [userId]);

        foundInitiative.save(function (err) {
          if (err) {
            console.log("error saving", err);
            return next(err);
          }
          //Initiative.publishUpdate(initiativeId,foundInitiative.toJSON() );

          Initiative.message(initiativeId
            , {
              action: "likeAdded",
              data: foundInitiative
            }
          );
          console.log("like added", userId);
          res.json({
            "initiative": foundInitiative
          });
        });
      }
    ).fail(function (err) {
        console.log("fail liking the initiative", err);

      });


  },
  dislikeInitiative: function (req, res, next) {
    var initiativeId = req.param("initiativeId");
    var userId = req.param("userId");

    Initiative.findOne()
      .where({ id: initiativeId })
      .then(function (foundInitiative, err) {

        if (err) {
          console.log("error searching", err, foundInitiative);
          return next(err);
        }
        if (!foundInitiative.userDislikesIds) {
          foundInitiative.userDislikesIds = new Array();
        }

        foundInitiative.userDislikesIds = underscore.union(foundInitiative.userDislikesIds, [userId]);

        foundInitiative.save(function (err) {
          if (err) {
            console.log("error saving", err);
            return next(err);
          }
          //Initiative.publishUpdate(initiativeId,foundInitiative.toJSON() );

          Initiative.message(initiativeId
            , {
              action: "dislikeAdded",
              data: foundInitiative
            }
          );
          console.log("dislike added", userId);
          res.json({
            "initiative": foundInitiative
          });
        });
      }
    ).fail(function (err) {
        console.log("fail liking the initiative", err);

      });
  },
  jsonld: function (req, res, next) {
    var initiativeId = req.param('id');
    console.log("getting linked data", initiativeId);
    var queryObject = {};

    if (initiativeId){
      queryObject = { id: initiativeId };
    }

    Initiative.find()
      .where(queryObject)
      .then(addContextToList)
      .then(jsonld.expand)
      .then(function (recordExpanded) {
        console.log("linked Data expanded", recordExpanded);
        res.json(
          recordExpanded
        );
      }
    );
  },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to InicitiveController)
   */
  _config: {
  }


};
