/**
 * UserController
 *
 * @module      :: Controller
 * @description  :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var underscore = require("underscore");
var jsonld = require('jsonld').promises;

var context = {
  "foaf" : "http://xmlns.com/foaf/0.1/",
  "person": "foaf:person",
  "name": "foaf:firstName",
  "lastName": "foaf:lastName",
  "identification": "foaf:account",
  "email": "foaf:mbox",
  "role": "foaf:member"
};

var addContextToList = function (list) {
  console.log("adding context to list", list);
  underscore.each(list, function (item) {
    addContext(item)
  });
  console.log("addedcontext to list", list);
  return list;
};

var addContext = function (user) {
  console.log('adding Context', user);
  user["@context"] = context;
  user["@type"] = 'person';
  user["@id"] = 'https://legislator.com/resources/user/' + user.id;

  console.log('user with Context', user);
  return user;
};


module.exports = {
  view: function (req, res) {
    res.view(
      "user/view"
    );
  },
  userByAttribute: function (req, res, next) {
    var attribute = req.param('attribute');
    var queryAttribute = req.param('queryattribute');
    var queryObject = {};

    console.log("userByAttribute atribute ", attribute, " queryAttribute", queryAttribute);

    objRegex = new RegExp('^' + queryAttribute + ".*" + '$', 'i');
    queryObject[attribute] = { $regex: objRegex  };
    //console.log("usuarios"+JSON.stringify(users));

    var users = User.find()
      .where(queryObject)
      .sort(attribute)
      .paginate({page: 0, limit: 10})
      .exec(
      function (err, users) {
        if (err) {
          console.log("Error searching by ", attribute, " : ", queryAttribute);
          next(err);
        } else {
          res.json(
            users
          );
        }
      }
    );


    /*    User.native( function( err, user ){

     if( err ) return err;
     var resultado = user.find( {  }

     ).toArray(function(err, results) {
     console.log("resultados", results, err);
     });
     console.log("usuarios"+JSON.stringify(resultado));
     }
     );*/
  },
  jsonld: function (req, res, next) {
    var userId = req.param('id');

    console.log("findUsersInformation userId ", userId);
    var queryObject = {};

    if (userId){
      queryObject = { id: userId };
    }

    User.find()
      .where(queryObject)
      .then(addContextToList)
      .then(jsonld.expand)
      .then(function (recordExpanded) {
        console.log("linked Data expanded", recordExpanded);
        res.json(
          recordExpanded
        );
      });
  },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {}


};
