/**
 * ArticleController
 *
 * @module      :: Controller
 * @description  :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var underscore = require('underscore');
var jsonld = require('jsonld').promises;

var context = {
  "legislator": "https://legislator.com/resources/",
  "foaf": "http://xmlns.com/foaf/0.1/",
  "metalex": "http://www.metalex.eu/metalex/2008-05-02#",
  "dct": "http://purl.org/dc/terms/",
  "xsd": "http://www.w3.org/2001/XMLSchema#",
  "opmv": "http://purl.org/net/opmv/ns#",
  "text": "dct:text",
  "initiativeId": {"@id": "legislator:initiative", "@type": "@id"},
  "title": "dct:title",
  "articleNumber": "xsd:integer",
  "currentVersion": "xsd:string",
  "problem": "dct:subject",
  "textOfModification": "dct:subject",
  "author": "metalex:realizedBy",
  "versions": "legislator:articleVersion",
  "comments": "legislator:comments",
  "homepage": "foaf:homepage",
  "page": "foaf:page",
  "createdAt":"opmv:wasGeneratedAt"
};

var addContext = function (article) {
  console.log('adding Context', article);

  article["@context"] = context;
  article["@type"] = 'text';
  article["@id"] = 'https://legislator.com/resources/article/' + article.id;
  article["page"] = 'https://legislator.com/page/initiative/' + article.id;
  article["homepage"] = 'https://legislator.com/resources/initiative/' + article.id;
  console.log('user with Context', article);
  return article;
};

var addContextToList = function (list) {
  console.log("adding context to list", list);
  underscore.each(list, function (item) {
    addContext(item)
  });
  console.log("addedcontext to list", list);
  return list;
};

module.exports = {

  list: function (req, res, next) {
    res.view(
      {
        actionBarLayout: sails.config.actionLayout.initiative.editArticles
      },
      "article/list"
    );
  },
  edit: function (req, res, next) {
    res.view(
      {
        actionBarLayout: sails.config.actionLayout.initiative.editArticles
      },
      "article/edit"
    );
  },
  loadArticleByIds: function (req, res, next) {
    var initiativeId = req.param("initiativeId");
    var articleId = req.param("articleId");
    var queryObject;
    Article.watch(req.socket);

    queryObject = {
      initiativeId: initiativeId,
      id: articleId
    };
    Article.findOne(
      queryObject
    ).exec(function (err, article) {
        if (err) {
          console.log("Error searching ", queryObject);
          next(err);
        } else {
          console.log("articles", article, err);
          res.json(
            article
          );
        }

      });
  },
  loadArticlesByInitiative: function (req, res, next) {
    var initiativeId = req.param("initiativeId");

    Article.watch(req.socket);

    Article.find({
        initiative: initiativeId
      }
    ).sort({
        articleNumber: "asc"
      }
    ).exec(function (err, articles) {
        if (err) {
          console.log("Error searching ", queryObject);
          next(err);
        } else {
          console.log("articles", articles, err);
          res.json(
            articles
          );
        }

      });
  },
  create: function (req, res, next) {
    var initiativeId = req.param("initiativeId");


    var publishArticleCreation = function (article) {
      delete article.createdAt;
      delete article.updatedAt;

      Article.publishCreate(
        article.toJSON()
      );
    };

    var createArticle = function (initiativeId, newArticleNumber) {
      console.log("creando el articulo promesas", initiativeId, newArticleNumber);
      return Article.create({
          ofInitiative: initiativeId,
          articleNumber: newArticleNumber,
          title: res.__('NewArticle'),
          author: req.session.user.id
        }
      );
    };
    var searchAvailablePosition = function (array) {
      var availablePosition, currentPosition;
      if (array && !underscore.isEmpty(array)) {
        //find if there is an avaible position
        underscore.every(array, function (element, index, list) {
          currentPosition = index + 1;
          return currentPosition == element.articleNumber;
        });
        //it means that this position is available
        if (currentPosition != array.length || array[currentPosition - 1].articleNumber != currentPosition) {
          availablePosition = currentPosition;
        }
        else {
          //if finish in the last position means there is no available position except the next one 
          console.log("last position ", currentPosition);
          availablePosition = currentPosition + 1;
        }
      } else {
        //if is undefined or empty the available position is 1
        availablePosition = 1;
      }
      console.log("finish searching available position", availablePosition);
      return availablePosition;
    };

    var searchArticles = function (initiativeId) {
      var promise = Article.find({ ofInitiative: initiativeId})
        .sort(
        {
          articleNumber: 'asc'
        }
      );
      return promise;
    };

    var handleError = function (error) {
      return console.log(error);
    };

    var createNextArticle = function (initiativeId) {
      console.log("createNextArticle");
      searchArticles(initiativeId)
        .then(function (articles) {
          return [initiativeId , searchAvailablePosition(articles)];
        })
        .spread(createArticle)
        .then(publishArticleCreation)
        .fail(handleError);
    };
    createNextArticle(initiativeId);
  },
  jsonld: function (req, res, next) {
    var articleId = req.param('id');

    console.log("article id", articleId);

    var queryObject = {};

    if (articleId){
      queryObject = { id: articleId };
    }

    Article.find()
      .where(queryObject)
      .then(addContextToList)
      .then(jsonld.expand)
      .then(function (recordExpanded) {
        console.log("linked Data expanded", recordExpanded);
        res.json(
          recordExpanded
        );
      })
      .catch(console.log);
  },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ArticlesController)
   */
  _config: {}


};
