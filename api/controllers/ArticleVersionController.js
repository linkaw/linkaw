/**
 * ArticleController
 *
 * @module      :: Controller
 * @description  :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var underscore = require('underscore');
var jsonld = require('jsonld').promises;

var context = {
  "legislator" :"https://legislator.com/resources/",
  "foaf" : "http://xmlns.com/foaf/0.1/",
  "metalex": "http://www.metalex.eu/metalex/2008-05-02#",
  "dct": "http://purl.org/dc/terms/",
  "opmv": "http://purl.org/net/opmv/ns#",
  "xsd": "http://www.w3.org/2001/XMLSchema#",
  "text" : "dct:text",
  "articleNumber": "dct:integer",
  "ofArticle":"legislator:Article",
  "homepage": "foaf:homepage",
  "page": "foaf:page",
  "author": "metalex:realizedBy",
  "createdAt":"opmv:wasGeneratedAt"
};

var addContext = function (articleVersion) {
  console.log('adding Context', articleVersion);

  articleVersion["@context"] = context;
  articleVersion["@type"] = 'text';
  articleVersion["@id"] = 'https://legislator.com/resource/articleVersion/' + articleVersion.id;
  articleVersion["page"] = 'https://legislator.com/view/articleversion/' + articleVersion.id;
  articleVersion["homepage"] = 'https://legislator.com/resource/articleversion/' + articleVersion.id;

  console.log('user with Context', articleVersion);
  return articleVersion;
};

var addContextToList = function (list) {
  console.log("adding context to list", list);
  underscore.each(list, function (item) {
    addContext(item)
  });
  console.log("addedcontext to list", list);
  return list;
};

module.exports = {

  findOneLinked: function (req, res, next) {
    var articleVersionId = req.param('id');

    console.log("article version id", articleVersionId);

    ArticleVersion.findOne()
      .where({ id: articleVersionId })
      .then(addContext)
      .then(jsonld.expand)
      .then(res.json)
      .catch(console.log);
  },
  findLinked: function (req, res, next) {
    ArticleVersion.find()
      .then(addContextToList)
      .then(jsonld.expand)
      .then(res.json)
      .catch(console.log);
  },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ArticlesController)
   */
  _config: {}


};
