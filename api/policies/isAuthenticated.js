/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
  console.log("policy is authenticated");
  
  if (typeof req.session.passport.user !== "undefined"){
    console.log("policy authenticated"+req.session.passport.user);
    console.log(JSON.stringify(req.session.passport.user));
    return next();
  }else{
    console.log("policy not autenticated");
    res.redirect("/login");
    return;
   //return res.send(403, { message: 'Not Authorized' });

  }

  /*
  if (req.isAuthenticated()){
    console.log("policy authenticated");
    return next();
  }else{
    console.log("policy not autenticated");
    res.redirect("/signin");
    return;
   //return res.send(403, { message: 'Not Authorized' });

  }
  
  */

};
