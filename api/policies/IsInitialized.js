/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  console.log("policy initialized");
  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
  console.log("applying policy initialized");
  if (req.isAuthenticated() && typeof req.session.navbarlayout != "undefined"){
    req.session.navbarlayout=sails.config.layout.user;
    req.session.username =sails.config.user[0].name;
    req.session.authenticated=true;
    console.log("policy user initialized");
    return next();
  }else{
    req.session.navbarlayout=sails.config.layout.user;
    req.session.username = "Guest";
    req.session.authenticated=true;
    console.log("policy guest initialized");
    
    return next();
  }

};
