/**
 * isAuthor
 *
 * @module      :: Policy
 * @description :: Simple policy to allow edit only to Collaborators or Authors
 *
 */
module.exports = function(req, res, next) {
  console.log("policy canEdit");
  // id is author or collaborator, proceed to the next policy, 
  var initiativeId = req.param("initiativeId");
  
  ValidatorService.initiative.isAuthor(initiativeId, req.session.passport.user)
  .then(
    function(isAuthor){
      if(isAuthor){
        return next();
        console.log("es author");
      }else{
        console.log("no es author");
        ValidatorService.initiative.isCollaborator(initiativeId, req.session.passport.user)
        .then(
          function(isCollaborator){
            if(isCollaborator){
              return next();
            }else{
              console.log("no es colaborador error");
              res.redirect("/errormessage");
            };
          }
        );
      };
    }
  )

};
