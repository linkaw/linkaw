/**
 * isAuthor
 *
 * @module      :: Policy
 * @description :: Simple policy to allow delete only to Authors 
 *
 */
module.exports = function(req, res, next) {
  // id is author, proceed to the next policy, 
  console.log("policy canDelete");
  var initiativeId = req.param("parentid");
  ValidatorService.initiative.isAuthor(initiativeId, req.session.passport.user)
  .then(
    function(isAuthor){
      if(isAuthor){
        return next();
      }else{
        res.redirect("/errormessage");
      };
    }
  );

};
