
angular.module('legislator').controller('ChatPanelController', chatPanelController);

function chatPanelController($scope, $window, messageService, userService, $sails) {
  var setInitialVariables = function () {
    $scope.user = $window.exported.user;
    $scope.messages = {};
    if($scope.articleId){
      $scope.$watch("articleId", setArticleId);
    }else{
      $scope.$watch("initiativeId", setInitiativeId);
    }
  };

  function setInitiativeId(initiativeId) {
    $scope.initiativeId = initiativeId;
    getMessagesOfInitiative(initiativeId);
  }

  function setArticleId(articleId) {
    $scope.articleId = articleId;
    getMessagesOfArticle(articleId);
  }

  var listenSocketMessages = function () {
    $sails.on('message', function (publish) {
      console.log("mensaje cometa" + JSON.stringify(publish));
      var message = publish.data;
      if (publish.verb === "created") {
        console.log("creando" + JSON.stringify(message));
        message = populateUserToMessage(message);
        $scope.messages.push(message);
      }
    });

  };

  var populateUserToMessage= function (message){
    userService.getUser(message.writtenBy)
      .success(function(user){
        console.log('this is the user', user);
        message.writtenBy = user;
      });
    return message;
  };

  var getMessagesOfInitiative = function (initiativeId) {
    messageService.getMessagesOfInitiative(initiativeId)
      .success(function (messages) {
        console.log("messages loaded", messages);
        $scope.messages = messages;
      },
      function (error) {
        console.log("message fail", error);
      }
    )
  };

  var getMessagesOfArticle = function (articleId) {
    messageService.getMessagesOfArticle(articleId)
      .success(function (messages) {
        console.log("messages loaded", messages);
        $scope.messages = messages;

      },
      function (error) {
        console.log("message fail", error);
      }
    )
  };

  var addMessage = function (messageText) {
    if (messageText){
      $scope.messageText = "";
      messageService.addMessage(messageText, $scope.user, $scope.initiativeId, $scope.articleId)
        .success(
        function (message) {
          console.log("message added", JSON.stringify(message));
          message.writtenBy = $scope.user;
          $scope.messages.push(message);
        },
        function (error) {
          console.log("message fail", error);
        }
      )
    }

  };

  $scope.addMessage = addMessage;
  setInitialVariables();
  listenSocketMessages();
};