angular.module('legislator').controller('CollaboratorsPanelController', collaboratorsPanelController);

function collaboratorsPanelController($scope, $sails, userService, initiativeService, $state) {

  //No Exposed functions

  var setInitialVariables = function () {

  };

  $scope.goTouserPage = function (userId) {
    $state.go('legislator.user', {userId: userId});
  };

  var listenSocketMessages = function () {
    $sails.on('initiative', function messageReceived(publish) {
      console.log("mensaje cometa" + JSON.stringify(publish));
      if (publish.verb === "addedTo") {
        console.log("addedTo" + publish.addedId);
        populateUser(publish.addedId)
      }else if (publish.verb === "removedFrom") {
        console.log("removedFrom" + publish.removedId);
        $scope.initiative.collaborators.remove(function (user) {
          return user.id === publish.removedId;
        });
      }
    });
  };

  var populateUser= function (collaboratorId){
    userService.getUser(collaboratorId)
      .success(function(user){
        console.log('populating user', user);
        $scope.initiative.collaborators.push(user);
      });
    return comment;
  };

  var loadUsers = function (userNameSearchText) {
    console.log("loadUsers", userNameSearchText);
    if(userNameSearchText){
      userService.getUsersbyAttribute("name", userNameSearchText)
        .success(function (listOfUsersToAdd) {
          if (listOfUsersToAdd) {
            var alreadyCollaborators;
            alreadyCollaborators = [$scope.initiative.author].union($scope.initiative.collaborators);
            //remove the users that are collaborators
            alreadyCollaborators.each(function (collaborator, index) {
              listOfUsersToAdd.remove(function (user) {
                return user.id === collaborator.id;
              });
            });

            $scope.users = listOfUsersToAdd;

          } else {
            $scope.users = {};
          }
        });
    }else{
      $scope.users = {};
    }

  };

  var addCollaborator = function (collaboratorId) {
    console.log("addCollaborator");
    initiativeService.addCollaborator($scope.initiative.id, collaboratorId)
      .success(function (initiative) {
        $scope.initiative.collaborators = initiative.collaborators;
        //remover el usuario agregado de la lista de usuarios disponibles para agregar
        $scope.users.remove(function (user) {
          return user.id === collaboratorId;
        });
      }
    );

  };

  var removeCollaborator = function (collaboratorId) {
    console.log("removeCollaborator");
    initiativeService.removeCollaborator($scope.initiative.id, collaboratorId)
      .success(function (initiative) {
        $scope.initiative.collaborators = initiative.collaborators;
        console.log("removed", initiative);
      });
  };


  setInitialVariables();
  listenSocketMessages();

  $scope.loadUsers = loadUsers;
  $scope.addCollaborator = addCollaborator;
  $scope.removeCollaborator = removeCollaborator;

} 

