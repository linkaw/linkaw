angular.module('legislator').controller('CommentsPanelController', commentsPanelController);

function commentsPanelController($scope, $window, commentService, userService, $sails) {
  var setInitialVariables = function () {

    $scope.user = $window.exported.user;
    $scope.comments = {};
    $scope.comments = new Array();
    $scope.filteredStatus="";
    if($scope.articleId){
      $scope.$watch("articleId", setArticleId);
    }else{
      $scope.$watch("initiativeId", setInitiativeId);
    }
  };

  function setInitiativeId(initiativeId) {
    $scope.initiativeId = initiativeId;
    getCommentsOfInitiative(initiativeId);
  }

  function setArticleId(articleId) {
    $scope.articleId = articleId;
    getCommentsOfArticle(articleId);
  }

  var listenSocketComments = function () {
    $sails.on('comment', function (publish) {
      console.log("mensaje cometa" + JSON.stringify(publish));
      var comment = publish.data;
      if (publish.verb === "created") {
        console.log("creando" + JSON.stringify(comment));
        comment = populateUserToComment(comment);
        $scope.comments.push(comment);
      }
      if (publish.verb === "updated") {
        console.log("actualizando" + JSON.stringify(publish.data));
        $scope.comments.each(function (element, index, array) {
          if (element.id === publish.id) {
            comment = _.extend(publish.previous, publish.data);
            array[index] = comment;
            return false;
          }
        });
      }
      if (publish.verb === "destroyed") {
        console.log("deleting" + JSON.stringify(publish.data));
        $scope.comments.each(function (element, index, array) {
          if (element.id === publish.id) {
            array.splice(index,1);
            return false;
          }
        });
      }
    });

  };

  var populateUserToComment= function (comment){
    userService.getUser(comment.commentedBy)
      .success(function(user){
        console.log('this is the user', user);
        comment.commentedBy = user;
      });
    return comment;
  };

  var getCommentsOfInitiative = function (initiativeId) {
    commentService.getCommentsOfInitiative(initiativeId)
      .success(function (comments) {
        console.log("comments loaded", comments);
        $scope.comments = comments;
      },
      function (error) {
        console.log("comment fail", error);
      }
    )
  };

  var getCommentsOfArticle = function (articleId) {
    commentService.getCommentsOfArticle(articleId)
      .success(function (comments) {
        console.log("comments loaded", comments);
        $scope.comments = comments;

      },
      function (error) {
        console.log("comment fail", error);
      }
    )
  };

  var addComment = function (commentText) {
    if (commentText){
      $scope.commentText = "";
      $scope.filteredStatus='opened';
      commentService.addComment(commentText, $scope.user, $scope.initiativeId, $scope.articleId)
        .success(
        function (comment) {
          console.log("comment added", JSON.stringify(comment));
          comment.commentedBy = $scope.user;
          $scope.comments.push(comment);
        },
        function (error) {
          console.log("comment fail", error);
        }
      )
    }

  };

  var updateCommentStatus = function (comment, status) {
    commentService.updateCommentStatus(comment.id, status, comment.reply)
      .success(
      function (commentResult) {
        console.log("comment status updated", JSON.stringify(comment));
        comment.status = commentResult.status;
      },
      function (error) {
        console.log("comment fail", error);
      }
    )

  };

  var deleteComment = function (comment) {
    commentService.deleteComment(comment.id)
      .success(
      function (commentResult) {
        console.log("deleting comment", comment.id);
        $scope.comments.each(function (element, index, array) {
          if (element.id === comment.id) {
            array.splice(index,1);
            return false;
          }
        });
      }
    )

  };



  $scope.addComment = addComment;
  $scope.deleteComment = deleteComment;
  $scope.updateCommentStatus = updateCommentStatus;
  setInitialVariables();
  listenSocketComments();
};