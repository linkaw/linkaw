angular.module('legislator').controller('ActionBarController', function($scope, $window, linkawAppService) {

    $scope.toggleShowNavBar = function (){
      if(!linkawAppService.showNavBar){
        linkawAppService.toggleAllOff();
      }

      linkawAppService.showNavBar = !linkawAppService.showNavBar;
    };

    $scope.toggleAddCollaboratorBox  = function (){
      if(!linkawAppService.showCollaboratorBox){
        linkawAppService.toggleAllOff();
      }
      linkawAppService.showCollaboratorBox = !linkawAppService.showCollaboratorBox;
    };

    $scope.toggleChatBox  = function (){
      if(!linkawAppService.showMessagesBox){
        linkawAppService.toggleAllOff();
      }
      linkawAppService.showMessagesBox = !linkawAppService.showMessagesBox;
    };

    $scope.toggleCommentsBox = function (){
      if(!linkawAppService.showCommentBox){
        linkawAppService.toggleAllOff();
      }
      linkawAppService.showCommentBox = !linkawAppService.showCommentBox;
    };

  }
);
