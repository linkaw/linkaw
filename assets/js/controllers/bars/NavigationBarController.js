angular.module('legislator').controller('NavigationBarController', function ($scope, $window, $templateCache, $state, linkawAppService, $translate) {
    $scope.scrolled = false;

    $scope.otherLanguage = 'en';

    $scope.toggleLanguage = function () {
      $translate.use(getOtherLanguage($translate.use()));
      $scope.otherLanguage = $translate.use();
    };

    function getOtherLanguage(otherLanguage){
      return (otherLanguage === 'en') ? 'es' : 'en';
    }


    var setInitialVariables = function () {
      $scope.linkawAppService = {};
      $scope.linkawAppService = linkawAppService;
      if ($window.exported) {
        $scope.user = $window.exported.user;
        $scope.isAuthenticated = $window.exported.isAuthenticated;
        $scope.navBarLayout = $window.exported.userNavBarLayout;
      }
    };


    setInitialVariables();

    $scope.getUserImageUrl = function () {

      if (!$scope.user.photoUrl) {
        $scope.user.photoUrl = '/images/unkwoun.jpg'
      }

      return $scope.user.photoUrl;
    };

    $scope.logout = function () {

      $scope.user = {
        name: 'Desconocido',
        photoUrl: ''
      };

      $templateCache.removeAll();
      $state.go("homepage");

    };
  }
);
