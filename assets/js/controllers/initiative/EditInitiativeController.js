angular.module('legislator').controller('EditInitiativeController', EditInitiativeController);

function EditInitiativeController($scope, $window, $sails, $log, $stateParams, initiativeService, articleService, linkawAppService) {


  var setInitialVariables = function () {
    $scope.initiative = {};
    $scope.linkawAppService = {};
    $scope.linkawAppService = linkawAppService;
    $scope.initiativeId = $stateParams.initiativeId;
    $scope.articleList = [];
    $log.debug("Initial Settings", $scope.initiativeId);
    $scope.user = $window.exported.user;
  };

  var loadArticles = function (initiativeId) {
    $log.debug("loadArticles ", initiativeId);
    articleService.getArticlesOfInitiative(initiativeId)
      .success(function (response) {
        $scope.articleList = response;
        $log.debug("finish loading Articles", response);
      });
  };

  var deleteArticle = function (articleId) {
    $log.debug("delete article ", articleId);

    articleService.deleteArticle(articleId)
      .success(function (article) {
        $scope.articleList.each(function (element, index, array) {
          if (element.id === article.id) {
            array.splice(index,1);
            return false;
          }
        });
        $log.debug("finish loading Articles", response);
      });
  };


  var loadInitiativeInfo = function (initiativeId) {
    $log.debug("loadInitiativeInfo", initiativeId);
    initiativeService.getInitiative(initiativeId)
      .success(function (initiative) {
        $scope.initiative = initiative;
        loadArticles(initiative.id);
      });
  };


  var listenSocketMessages = function () {
    $sails.on('article', function messageReceived(publish) {
        //listes for articles messages
        if (publish.verb === "created") {
          $scope.articleList.push(publish.data);
        }

        if (publish.verb === "destroyed") {
          $scope.articleList.remove(function (obj) {
            return obj.id === publish.id;
          });
        }

        if (publish.verb === "updated") {
          $scope.articleList.each(function (element, index, array) {
            if (element.id === publish.id) {
              array[index] = publish.data;
              return false;
            }
          });
        }

      });

    $sails.on('initiative', function messageReceived(publish) {
      if (publish.verb === "destroyed") {
        goToMainPage();
      }
      if (publish.verb === "updated") {
        $scope.initiative = publish.data;
      }

    });
  };

  //Exposed functions
  var toggleAddCollaboratorBox = function () {
    toggleBox("0");
  };
  var toggleChatBox = function () {
    toggleBox("1");
  };
  var toggleCommentsBox = function () {
    toggleBox("2");
  };

  var toggleBox = function (boxName) {
    if (!$scope.interactiveSelected) {
      $scope.interactiveSelected = new Array();
    };

    if ($scope.visibilityClass == 'verticalShowed' && $scope.interactiveSelected[boxName] == 'verticalShowed') {
      $scope.interactiveSelected[boxName] = 'verticalHidden'
      $scope.visibilityClass = 'verticalHidden';

    } else {
      $scope.interactiveSelected.each(function (element, index) {
        $scope.interactiveSelected[index] = 'verticalHidden';
        console.log("vertical hidden", $scope.interactiveSelected[index]);
      });
      console.log("vertical hidden", $scope.interactiveSelected);

      $scope.visibilityClass = 'verticalShowed';
      $scope.interactiveSelected[boxName] = 'verticalShowed'

    }

  };

  var toggleInteractionBox = function () {
    console.log("toggling class", $scope.visibilityClass);
    if ($scope.visibilityClass == 'verticalHidden') {
      $scope.visibilityClass = 'verticalShowed';
    } else {
      $scope.visibilityClass = 'verticalHidden';
    }
  };


  var saveHeader = function () {
    //saving Header
    $log.debug("updateInitiative");
    initiativeService.updateInitiative($scope.initiative)
      .success(function (response) {
        $log.debug("initiative updated", response);
      });
  };


  var createArticle = function () {
    articleService.createArticle($scope.initiativeId)
      .success(function (response) {
        $log.debug("article created", response);
      });
  };


  var setCurrentArticle = function (article) {
    $log.debug("article", article);
    $scope.shareDocumentName = "Initiative" + article.initiativeId + "Article" + article.id;
    $scope.currentArticle = article;
  };

  var goToLinkedData = function(){
    $window.location.href = "/resource/initiative/"+$scope.initiativeId;
  };

  var goToMainPage = function(){
    $window.location.href = "/";
  };

  setInitialVariables();
  loadInitiativeInfo($scope.initiativeId);
  listenSocketMessages();

  $scope.visibilityClass = 'verticalHidden';
  $scope.toggleAddCollaboratorBox = toggleAddCollaboratorBox;
  $scope.toggleChatBox = toggleChatBox;
  $scope.toggleCommentsBox = toggleCommentsBox;
  $scope.deleteArticle = deleteArticle;
  $scope.saveHeader = saveHeader;
  $scope.createArticle = createArticle;
  $scope.setCurrentArticle = setCurrentArticle;
  $scope.goToLinkedData=goToLinkedData;

  $window.onbeforeunload = function () {
    //return "Al salir de esta pagina no se guardaran los cambios hechos al documento";
  };


  $scope.toggleAddCollaboratorBox  = function (){
    if(!linkawAppService.showCollaboratorBox){
      linkawAppService.toggleAllOff();
    }
    linkawAppService.showCollaboratorBox = !linkawAppService.showCollaboratorBox;
  };

  $scope.toggleChatBox  = function (){
    if(!linkawAppService.showMessagesBox){
      linkawAppService.toggleAllOff();
    }
    linkawAppService.showMessagesBox = !linkawAppService.showMessagesBox;
  };

  $scope.toggleCommentsBox = function (){
    if(!linkawAppService.showCommentBox){
      linkawAppService.toggleAllOff();
    }
    linkawAppService.showCommentBox = !linkawAppService.showCommentBox;
  };

} 

