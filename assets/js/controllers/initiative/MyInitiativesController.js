
angular.module('legislator').controller('MyInitiativesController', function ($scope, $sails, $cookieStore, $window, $mdToast, initiativeService, userService) {

    var setInitialVariables = function () {
      $scope.user = $window.exported.user;
      console.log("userid al settear variables", $scope.user.id);
    };

    var loadMyInitiatives = function (authorId) {

      initiativeService.loadInitiativesByAuthor(authorId)
        .success(function (initiativeList) {
          console.log("callback de socket" + JSON.stringify(initiativeList));
          $scope.myInitiativeList = initiativeList;
        }
      );
    };

    var loadSharedInitiatives = function (authorId) {
      userService.getUser(authorId)
        .success(function (user) {
          console.log("callback de socket" + JSON.stringify(user));
          $scope.user = user;
          $scope.sharedInitiativeList = user.colaboratorOf;
        }
      );
    };

    var listenSocketMessages = function () {
      $sails.on('initiative', function messageReceived(message) {
          console.log("mensaje cometa" + JSON.stringify(message));
          if (message.verb === "created") {

            $scope.myInitiativeList.push(message.data);
          }

          if (message.verb === "destroyed") {
            $scope.myInitiativeList.remove(function (obj) {
              return obj.id === message.id;
            });
          }

          if (message.verb === "updated") {
            $scope.myInitiativeList.each(function (element, index, array) {
              if (element.id === message.id) {
                array[index] = message.data;
                return false;
              }
            });
          }
        }
      );

      $sails.on('user', function messageReceived(message) {
          console.log("mensaje cometa" + JSON.stringify(message));
          if (message.attribute === "colaboratorOf") {
            if (message.verb === "addedTo") {
              initiativeService.getInitiative(message.addedId)
                .success(function (initiative) {
                  $scope.sharedInitiativeList.push(initiative);
                });
            } else if (message.verb === "removedFrom") {
              $scope.sharedInitiativeList.remove(function (obj) {
                return obj.id === message.removedId;
              });
            }

          }
        }
      );
    };

    //functions to expose
    var deleteItem = function (initiative) {
      initiative.isDeleting=true;
      var initiativeId = initiative.id;
      console.log("callback borrando");
      initiativeService.deleteInitiative(initiativeId)
        .success(function (initiative) {
          $scope.myInitiativeList.remove(function (obj) {
            return obj.id === initiative.id;
          });
        });
    };

    //logic at start
    setInitialVariables();
    loadMyInitiatives($scope.user.id);
    loadSharedInitiatives($scope.user.id);
    listenSocketMessages();

    //expose functions
    $scope.deleteItem = deleteItem;

    $scope.actionItems = [
      {
        item: "New",
        i18nKey: "New",
        action: null,
        image: "fa fa-plus-square",
        href: "/createeditinitiative"
      }
    ];
  }
);