
angular.module('legislator').controller('InitiativesController', function ($scope, $sails, $window, initiativeService) {

  $scope.showSearchBox = false;

  if ($window.exported) {
    $scope.user = $window.exported.user;
  }

  console.log("user id", $scope.user);

  console.log("antes de usar el socket");

  initiativeService.getInitiatives()
    .success(function (initiativeList) {
      console.log("initiativeList" + JSON.stringify(initiativeList));
      $scope.initiativeList = initiativeList;
    });

  $scope.filterInitiatives = function (filter) {
    console.log("filtering initiatives with ", filter);
    if(filter){
      initiativeService.loadInitiativesRelatedTo(filter)
        .success(function (initiativeList) {
          console.log("initiativeList" + JSON.stringify(initiativeList));
          $scope.initiativeList = initiativeList;
        });
    }else{
      initiativeService.getInitiatives()
        .success(function (initiativeList) {
          console.log("initiativeList" + JSON.stringify(initiativeList));
          $scope.initiativeList = initiativeList;
        });
    }

  };

  $scope.hasVoted = function (initiative) {
    var allVotedUserIds = _.union(initiative.userLikesIds, initiative.userDislikesIds);
    var hasVoted = _.contains(allVotedUserIds, $scope.user.id);
    console.log("has voted  ", hasVoted);
    return hasVoted;

  };

  $scope.likeItem = function (initiativeId, initiative) {
    console.log("liking item", initiative, initiativeId, $scope.user.id);

    initiativeService.addLike(initiativeId, $scope.user.id)
      .success(function (response) {
        console.log("added like", response);
      });
  };

  $scope.dislikeItem = function (initiativeId, initiative) {
    console.log("disliking item", initiative, initiativeId, $scope.user.id);

    initiativeService.addDislike(initiativeId, $scope.user.id)
      .success(function (response) {
        console.log("added dislike", response);
      });
  };

  $sails.on('initiative', function messageReceived(publish) {
      console.log("mensaje cometa" + JSON.stringify(publish));
      //custom message
      if (publish.verb === "messaged") {
        var message = publish.data;

        if (message.action === "likeAdded") {
          console.log("actualizando likes" + JSON.stringify(message.data));
          var initiative = _.find($scope.initiativeList,
            function (initiative) {
              return initiative.id === message.data.id;
            });
          initiative = _.extend(initiative, message.data);
        }

        if (message.action === "dislikeAdded") {
          console.log("actualizando dislikes" + JSON.stringify(message.data));
          var initiative = _.find($scope.initiativeList,
            function (initiative) {
              return initiative.id === message.data.id;
            });
          initiative = _.extend(initiative, message.data);
        }
      }

      if (publish.verb === "created") {
        $scope.initiativeList.push(publish.data);
      }
      if (publish.verb === "destroyed") {
        $scope.initiativeList.remove(function (obj) {
          return obj.id === publish.id;
        });
      }

      if (publish.verb === "updated") {
        $scope.initiativeList.each(function (element, index, array) {
          if (element.id === publish.id) {
            array[index] = publish.data;
            return false;
          }
        });
      }
    }
  );


  $scope.actionItems = [
    {
      item: "New",
      i18nKey: "New",
      action: null,
      image: "fa fa-plus-square",
      href: "/createeditinitiative"
    }
  ];

});