angular.module('legislator').controller('EditArticleController', function ($scope, $window, $sails, $stateParams, $log, $state, linkawAppService, articleService, articleVersionService, userService) {

    var setInitialVariables = function () {
      $scope.linkawAppService = {};
      $scope.linkawAppService = linkawAppService;
      $scope.user = $window.exported.user;
      $scope.initiativeId = $stateParams.initiativeId;
      $scope.articleId = $stateParams.articleId;
      $log.debug("Initial Settings initiative", $scope.initiativeId, "Article", $scope.articleId);
    };

    var loadArticleInfo = function (articleId) {
      return articleService.getArticle(articleId)
        .success(function (response) {
          $scope.article = response;
          $log.debug("finish loading Article Info", $scope.article);
        });
    };

    var loadArticleVersionsInfo = function (article) {
      return articleVersionService.getArticleVersionsOfArticle(article.id)
        .success(function (response) {
          $scope.article.versions = response;
          $log.debug("finish loading ArticleVersiona Info", $scope.article);

          if ($scope.article.versions) {
            $scope.versionNumber = $scope.article.versions.length - 1;
          }
        });
    };
    var listenSocketMessages = function () {
      $sails.on('articleversion', function messageReceived(publish) {
          $log.debug("mensaje cometa" + JSON.stringify(publish));
          if (publish.verb === "created") {
            var articleVersion = publish.data;
            pushArticleVersion(articleVersion);
          }
        }
      );
    };

    //Exposed functions
    var toggleChatBox = function () {
      toggleBox("0");
    };

    var toggleCommentsBox = function () {
      toggleBox("1");
    };

    var toggleBox = function (boxName) {
      if (!$scope.interactiveSelected) {
        $scope.interactiveSelected = [];
      }


      if ($scope.visibilityClass == 'verticalShowed' && $scope.interactiveSelected[boxName] == 'verticalShowed') {
        $scope.interactiveSelected[boxName] = 'verticalHidden';
        $scope.visibilityClass = 'verticalHidden';

      } else {
        $scope.interactiveSelected.each(function (element, index) {
          $scope.interactiveSelected[index] = 'verticalHidden';
          $log.debug("vertical hidden", $scope.interactiveSelected[index]);
        });
        $log.debug("vertical hidden", $scope.interactiveSelected);

        $scope.visibilityClass = 'verticalShowed';
        $scope.interactiveSelected[boxName] = 'verticalShowed'

      }

    };

    var saveArticle = function () {
      articleService.updateArticle($scope.article)
        .success(function(article){
          $log.debug("article updated", article);
        });
    };

    var saveVersion = function () {
      if ($scope.article.versions.length == 0 || $scope.article.currentVersion != $scope.article.versions[$scope.article.versions.length-1].text){
        articleVersionService.createArticleVersion($scope.article.currentVersion, $scope.articleId, $scope.user.id)
          .success(pushArticleVersion);
      }
    };

    function pushArticleVersion(articleVersion){
      userService.getUser(articleVersion.author)
        .success(function(user){
          articleVersion.author = user;
          $scope.article.versions.push(articleVersion);
          $scope.versionNumber = $scope.article.versions.length -1 ;
        });
    }

    var goToLinkedData = function () {
      $window.location.href = "/resource/article/" + $scope.articleId;
    };

    var goToInitiative = function () {
      $state.go('legislator.editInitiative', {initiativeId : $scope.initiativeId})
    };

    setInitialVariables();
    loadArticleInfo($scope.articleId)
      .success(loadArticleVersionsInfo);
    listenSocketMessages();

    $scope.goToInitiative = goToInitiative;
    $scope.saveArticle = saveArticle;
    $scope.saveVersion = saveVersion;
    $scope.toggleChatBox = toggleChatBox;
    $scope.toggleCommentsBox = toggleCommentsBox;
    $scope.goToLinkedData = goToLinkedData;

  }
);