
angular.module('legislator').controller('UserController', function ($scope, $sails, $cookieStore, $window, $stateParams, initiativeService, userService) {

    var setInitialVariables = function () {
      $scope.user = $window.exported.user;
      $scope.userToInspectId = $stateParams.userId;
      console.log("settear variables", $window.exported.user);
      console.log("user in the url", $scope.userToInspectId);
      userService.getUser($scope.userToInspectId)
        .success(function (user) {
          $scope.userToInspect = user;
        });


      console.log("userid al settear variables", $scope.user);
      console.log("userid al settear variables", $scope.user.id);

    };


    var loadMyInitiatives = function (authorId) {

      initiativeService.loadInitiativesByAuthor(authorId)
        .success(function (initiativeList) {
          console.log("callback de socket" + JSON.stringify(initiativeList));
          $scope.myInitiativeList = initiativeList;
        }
      );
    };

    var loadSharedInitiatives = function (authorId) {
      userService.getUser(authorId)
        .success(function (user) {
          console.log("callback de socket" + JSON.stringify(user));
          $scope.user = user;
          $scope.sharedInitiativeList = user.colaboratorOf;
        }
      );
    };

    $scope.hasVoted = function (initiative) {
      var allVotedUserIds = _.union(initiative.userLikesIds, initiative.userDislikesIds);
      var hasVoted = _.contains(allVotedUserIds, $scope.user.id);
      console.log("has voted  ", hasVoted);
      return hasVoted;

    };

    $scope.likeItem = function (initiativeId, initiative) {
      console.log("liking item", initiative, initiativeId, $scope.user.id);

      initiativeService.addLike(initiativeId, $scope.user.id)
        .success(function (response) {
          console.log("added like", response);
        });
    };

    $scope.dislikeItem = function (initiativeId, initiative) {
      console.log("disliking item", initiative, initiativeId, $scope.user.id);

      initiativeService.addDislike(initiativeId, $scope.user.id)
        .success(function (response) {
          console.log("added dislike", response);
        });
    };

    $sails.on('initiative', function messageReceived(publish) {
        console.log("mensaje cometa" + JSON.stringify(publish));
        //custom message
        if (publish.verb === "messaged") {
          var message = publish.data;

          if (message.action === "likeAdded") {
            console.log("actualizando likes" + JSON.stringify(message.data));
            var initiative = {};
            initiative = _.find($scope.myInitiativeList,
              function (initiative) {
                return initiative.id === message.data.id;
              });
            initiative = _.extend(initiative, message.data);

            initiative = _.find($scope.sharedInitiativeList,
              function (initiative) {
                return initiative.id === message.data.id;
              });

            initiative = _.extend(initiative, message.data);
          }

          if (message.action === "dislikeAdded") {
            console.log("actualizando dislikes" + JSON.stringify(message.data));
            var initiative = {};
            initiative = _.find($scope.myInitiativeList,
              function (initiative) {
                return initiative.id === message.data.id;
              });
            initiative = _.extend(initiative, message.data);

            initiative = _.find($scope.sharedInitiativeList,
              function (initiative) {
                return initiative.id === message.data.id;
              });
            initiative = _.extend(initiative, message.data);
          }
        }

        if (publish.verb === "created") {
          $scope.initiativeList.push(publish.data);
        }
        if (publish.verb === "destroyed") {
          $scope.initiativeList.remove(function (obj) {
            return obj.id === publish.id;
          });
        }

        if (publish.verb === "updated") {
          $scope.initiativeList.each(function (element, index, array) {
            if (element.id === publish.id) {
              array[index] = publish.data;
              return false;
            }
          });
        }
      }
    );

    //logic at start
    setInitialVariables();
    loadMyInitiatives($scope.userToInspectId);
    loadSharedInitiatives($scope.userToInspectId);

  }
);