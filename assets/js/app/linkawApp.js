var legislatorApp = angular.module('legislator', ['ngMaterial', 'ui.router' , 'ngSails', 'ngShare','ngDiff','crudModule',
  'luegg.directives', 'pascalprecht.translate', 'ngCookies', 'pc035860.scrollWatch',
  'actionBar', 'navigationBar', 'loginForm', 'socialLogin',
  'collaboratorsPanel', 'commentsPanel', 'chatPanel']);

legislatorApp.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $translateProvider) {

  var cadetblue = $mdThemingProvider.extendPalette('cyan', {
    '300': '5F9EA0'
  });
  $mdThemingProvider.definePalette('cadetblue', cadetblue);


  $mdThemingProvider.theme('default')
    .primaryPalette('cadetblue', {
      'default': '300',
      'hue-1': '100',
      'hue-2': '600',
      'hue-3': 'A100'
    })
    .accentPalette('grey')
    .warnPalette('blue-grey');

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $stateProvider
    .state('legislator', {
      url: "/",
      templateUrl: "/templates/homepage.html"
    })
    .state('initiatives', {
      url: "/initiatives",
      templateUrl: "/templates/initiative/list.html"
    })
    .state('myInitiatives', {
      url: "/myInitiatives",
      templateUrl: "/templates/initiative/mylist.html"
    })
    .state('sharedInitiatives', {
      url: "/sharedInitiatives",
      templateUrl: "/templates/initiative/sharedlist.html"
    })
    .state('editInitiative', {
      url: "/edit/:initiativeId",
      templateUrl: "/templates/initiative/edit.html"
    })
    .state('viewInitiative', {
      url: "/view/:initiativeId",
      templateUrl: "/templates/initiative/view.html"
    })
    .state('editArticle', {
      url: "/edit/:initiativeId/article/:articleId",
      templateUrl: "/templates/article/edit.html"
    })
    .state('legislator.user', {
      url: "/user/:userId",
      templateUrl: "/template/user"
    })
    .state('legislator.linkedData', {
      url: "/linkeddata",
      templateUrl: "/template/linkeddata"
    })
    .state('legislator.tutorial', {
      url: "/tutorial",
      templateUrl: "/template/manual"
    })
  ;

  $urlRouterProvider.otherwise('/');

  $translateProvider.useUrlLoader("/api/language");
  $translateProvider.preferredLanguage("en");

});
