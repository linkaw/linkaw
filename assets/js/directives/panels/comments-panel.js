// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var vegas = angular.module('commentsPanel', []);

vegas.directive('commentsPanel', function() {
  return {
    // Restrict it to be an attribute in this case
    restrict: 'E',
    scope: false,
    controller: 'CommentsPanelController',
    templateUrl : '/templates/panels/comments-panel.html'
  };
});

 