// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var vegas = angular.module('navigationBar', []);

vegas.directive('navigationBar', function($document) {
  return {
    restrict: 'E',
    scope: {
      items: '='
    },
    controller: 'NavigationBarController',
    templateUrl : '/templates/bars/navigationBar.html'
  };
});

 