// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var vegas = angular.module('actionBar', []);

vegas.directive('actionBar', function() {
  return {
    // Restrict it to be an attribute in this case
    restrict: 'E',
    scope: {
      items: '='
    },
    controller: 'ActionBarController',
    templateUrl : '/templates/bars/actionBar.html'
  };
});

 