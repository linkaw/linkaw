// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var ngDiff = angular.module('ngDiff', []);

ngDiff.directive('dirTextDiff', function () {
  return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var createDiff = function(initialModel,finalModel){
          console.log("iniciando diff",initialModel, finalModel);
          var dmp = new diff_match_patch();
          var diffs = dmp.diff_main( initialModel, finalModel);
          dmp.diff_cleanupSemantic(diffs);
          element[0].innerHTML = "";
          var htmlDiff = "";
          diffs.each(function(element){            
            if(element[0] == 0){
              htmlDiff = htmlDiff+element[1];
            }else if(element[0] == 1){
              htmlDiff = htmlDiff+"<span class='highlightAdded'>"+element[1]+"</span>";
            }else if(element[0] == -1){
              htmlDiff = htmlDiff+"<span class='highlightRemoved'>"+element[1]+"</span>";
            }
          });
          element[0].innerHTML=htmlDiff;
        };

        Object.watch(attrs,"dirFinalModel",function(prop, oldVal, newVal){
          createDiff(newVal, attrs.dirInitialModel);
          return newVal;
        });
        Object.watch(attrs,"dirInitialModel",function(prop, oldVal, newVal){
          createDiff(attrs.dirFinalModel, newVal);
          return newVal;
        });        

      }
    }
});


 