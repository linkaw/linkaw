// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var ngShare = angular.module('ngShare', []);

ngShare.directive("dirContentEditable", function($window) {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        console.log("poniendo text",element[0].innerText);
        console.log("en ",ngModel.$viewValue);
        ngModel.$setViewValue(element[0].innerText.replace(/(\r\n|\n|\r)/gm,""));
      }

      ngModel.$render = function() {
        element.html(ngModel.$viewValue || "");
      };


      element.bind("blur keydown keypress keyup change", function() {
        console.log("inner text",element);
        scope.$apply(read);
      });
    }
  };
});

ngShare.directive('dirShareModel', function () {
  return {
      restrict: 'A',
      scope:true,
      require: '^ngModel',
      controller: ['$scope', '$window','$parse', function($scope, $window) {
        console.log("controller article",$scope.article);
        $scope.shareModel = function(docName, ngModel) {
          $window.sharejs.open(docName, 'text', function(error, doc) {
            console.log("opening document",docName,doc);
            if (error) {
              console.log("ERROR:", error);
            } else {
              console.log("attaching doc");
              console.log("ngModel dentro",$scope);
              console.log("ngModel article",$scope.article);
              var ngModelSplited = ngModel.split(".");
              console.log("ngModel splited", ngModelSplited);
              var objectText = ngModel.slice(0,ngModel.length-ngModelSplited[ngModelSplited.length-1].length-1);
              console.log("ngModel objectText", objectText);
              var objectContainer = eval("$scope."+objectText);
              console.log("ngModel objectContainer", objectContainer);
              var property = ngModelSplited[ngModelSplited.length-1];
              console.log("ngModel objectProperty", objectContainer, property);
              doc.attach_model(objectContainer, property ,function(){

                //ngModel.$setViewValue(ngModel.$modelValue);
                //ngModel.$render(function(){console.log("termine de renderizar")});
                $scope.$apply();

              });
              console.log("finish opening document",docName,doc);
            }
          });
        }

      }],
      link: function($scope, $element, $attrs, $ngModel) {
        //$attrs.dirShareModel

        console.log("attributes ",$scope, $element, $attrs, $ngModel);
        console.log("attributes ",$ngModel);
        console.log("ngModel ",$scope);
        console.log("ngModel ",$scope.initiativeId,$scope.ngModel, $scope["ngModel"]);
        console.log("ngModel parsed",$attrs.ngModel,$scope.$eval($attrs.ngModel));
        console.log("link article",$scope.article);
        console.log("nombre documento",$attrs.dirShareModel);
        $scope.shareModel($attrs.dirShareModel, $attrs.ngModel);

      }
    }
});


ngShare.directive('dirShareTextArea', function () {
  return {
    restrict: 'A',
    scope:true,
    require: '^ngModel',
    controller: ['$scope', '$window','$parse', function($scope, $window) {
      console.log("controller article",$scope.article);
      $scope.shareTextArea = function(docName, element, ngModel) {
        $window.sharejs.open(docName, 'text', function(error, doc) {
          console.log("opening document",docName,doc);
          if (error) {
            console.log("ERROR:", error);
          } else {
            doc.attach_textarea(element, ngModel);
            console.log("finish opening document",docName,doc);
          }
        });
      }

    }],
    link: {
      post: function preLink($scope, $element, $attrs, $ngModel) {


        var unregister =$scope.$watch($attrs.ngModel.split('.')[0], function(newValue, oldValue) {

          if(oldValue != newValue && (!!newValue)){
            $scope.shareTextArea($attrs.dirShareTextArea, $element[0], $ngModel);
            unregister();
          }
        });

      }
    }
  }
});


