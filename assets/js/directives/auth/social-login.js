// The controller is a regular JavaScript function. It is called
// once when AngularJS runs into the ng-controller declaration.

var vegas = angular.module('socialLogin', []);

vegas.directive('socialLogin', function() {
  return {
    restrict: 'E',
    controller: 'LoginController',
    templateUrl : '/templates/auth/social-login.html'
  };
});

 