var crud = angular.module('crudModule', ['ngSails']);

crud.service('initiativeService', function ($sails) {

  this.getInitiative = function (initiativeId) {
    console.log("getInitiative", initiativeId);
    var promise = $sails.get('/api/initiative/' + initiativeId);
    return promise;
  };

  this.getInitiatives = function () {
    console.log("getInitiatives");
    var promise = $sails.get('/api/initiative/');
    return promise;
  };

  this.deleteInitiative = function (initiativeId) {
    console.log("deleteInitiative", initiativeId);
    var promise = $sails.delete('/api/initiative/' + initiativeId);
    return promise;
  };


  this.loadInitiativesRelatedTo = function (filter) {
    if (filter) {
      var listOfWords = filter.split(" ");
    }
    var listOfQueryTermsDescription = _.map(listOfWords, function (individualWord) {
      var queryTerm = {
        "description": { 'like': '%' + individualWord + '%'}
      };
      return queryTerm;
    });

    var listOfQueryTermsTitle = _.map(listOfWords, function (individualWord) {
      var queryTerm = {
        "title": { 'like': '%' + individualWord + '%'}
      };
      return queryTerm;
    });

    var listOfQueryTerms = _.union(listOfQueryTermsDescription, listOfQueryTermsTitle);

    var queryObject = {'or': listOfQueryTerms}
    console.log('query object' + JSON.stringify(queryObject));
    var promise = $sails.get('/api/initiative?where=' + JSON.stringify(queryObject));
    return promise;
  };

  this.loadInitiativesByAuthor = function (authorId) {
    var promise = $sails.get('/api/initiative?author=' + authorId);
    return promise;
  };

  this.updateInitiative = function (initiative) {
    //updating initiative
    console.log("updating initiative" + initiative);
    var promise = $sails.post('/api/initiative/' + initiative.id, initiative);
    return promise;
  };

  this.addCollaborator = function (initiativeId, collaboratorId) {
    console.log("addCollaborator", initiativeId, collaboratorId);
    var promise = $sails.post('/api/initiative/' + initiativeId + '/collaborators/' + collaboratorId);
    return promise;
  };

  this.removeCollaborator = function (initiativeId, collaboratorId) {
    console.log("removeCollaborator", initiativeId, collaboratorId);
    var promise = $sails.delete('/api/initiative/' + initiativeId + '/collaborators/' + collaboratorId);
    return promise;
  };

  this.addLike = function (initiativeId, userId) {
    //adding like to the initiative
    console.log("add Like to the initiative", initiativeId, userId);
    var promise = $sails.post('/editinitiative/' + initiativeId + '/like',
      {
        "userId": userId
      });
    return promise;
  };

  this.addDislike = function (initiativeId, userId) {
    //adding like to the initiative
    console.log("add disLike to the initiative", initiativeId, userId);
    var promise = $sails.post('/editinitiative/' + initiativeId + '/dislike',
      {
        "userId": userId
      });
    return promise;
  };
});


crud.service('userService', function ($sails) {

  this.getUser = function (userId) {
    console.log("getUser", userId);
    var promise = $sails.get('/api/user/' + userId);
    return promise;
  };

  this.getUsersbyAttribute = function (attributeName, attributeText) {
    console.log("getUsersbyAttribute ", attributeName, attributeText);
    var promise = $sails.get('/user/' + attributeName + '/' + attributeText);
    return promise;
  };
});

crud.service('articleService', function ($sails) {

  this.createArticle = function (initiativeId) {
    console.log("createArticle", initiativeId);
    var promise = $sails.post("/editinitiative/" + initiativeId + "/articles/create");
    return promise;
  };

  this.deleteArticle = function (articleId) {
    console.log("delete article " + articleId);
    var promise = $sails.delete("/api/article/" + articleId);
    return promise;
  };

  this.updateArticle = function (article) {
    console.log("updating article " + JSON.stringify(article));
    var promise = $sails.post("/api/article/" + article.id, article);
    return promise;
  };

  this.getArticle = function (articleId) {
    console.log("get Article ", articleId);
    var promise = $sails.get("/api/article/" + articleId);
    return promise;
  };

  this.getArticlesOfInitiative = function (initiativeId) {
    var promise = $sails.get("/api/article?ofInitiative=" + initiativeId);
    return promise;
  };

});

crud.service('articleVersionService', function ($sails) {
  this.getArticleVersionsOfArticle = function (articleId) {
    console.log("get Article Versions of article ", articleId);
    var promise = $sails.get("/api/articleVersion/?ofArticle=" + articleId);
    return promise;
  };

  this.createArticleVersion = function (text, articleId, authorId) {
    console.log("createArticleVersion", text, articleId, authorId);
    var articleVersion = {
      "text" : text,
      "author" : authorId,
      "ofArticle" : articleId
    };
    var promise = $sails.post("/api/articleversion", articleVersion);
    return promise;
  };
});

crud.service('messageService', function ($sails) {

  this.addMessage = function (text, authorId, initativeId, articleId) {
    console.log("add message", text, authorId, initativeId, articleId);
    var message = {
      "text": text,
      "writtenBy": authorId,
      "ofInitiative": initativeId,
      "ofArticle": articleId
    };

    var promise = $sails.post("/api/message", message);
    return promise;
  };

  this.getMessagesOfInitiative = function (initiativeId) {
    var promise = $sails.get("/api/message?ofInitiative=" + initiativeId);
    return promise;
  };

  this.getMessagesOfArticle = function (articleId) {
    var promise = $sails.get("/api/message?ofArticle=" + articleId);
    return promise;
  };
});

crud.service('commentService', function ($sails) {

  this.addComment = function (text, authorId, initativeId, articleId) {
    console.log("add comment", text, authorId, initativeId, articleId);

    var comment = {
      "text": text,
      "status": "opened",
      "commentedBy": authorId,
      "ofInitiative": initativeId,
      "ofArticle": articleId
    };

    var promise = $sails.post("/api/comment", comment);
    return promise;
  };

  this.updateCommentStatus = function (commentId, status, reply) {
    console.log("add comment", commentId, status, reply);

    var commentStatus = {
      "status": status,
      "reply": reply
    };

    var promise = $sails.put("/api/comment/" + commentId, commentStatus
    );
    return promise;
  };

  this.deleteComment = function (commentId) {
    var promise = $sails.delete("/api/comment/" + commentId);
    return promise;
  };

  this.getCommentsOfInitiative = function (initiativeId) {
    var promise = $sails.get("/api/comment?ofInitiative=" + initiativeId);
    return promise;
  };

  this.getCommentsOfArticle = function (articleId) {
    var promise = $sails.get("/api/comment?ofArticle=" + articleId);
    return promise;
  };

});

crud.service('translationService', function ($sails) {
  this.getTranslation = function (language){
    var promise = $sails.get("/translation/"+language);
    return promise;
  }
});

crud.service('authenticationService', function ($sails) {
  this.logout = function (){
    var promise = $sails.get("/api/logout");
    return promise;
  }
});
