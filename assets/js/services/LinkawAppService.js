angular.module('legislator').service('linkawAppService', function () {
  this.showNavBar = true;
  this.showCollaboratorBox = false;
  this.showMessagesBox = false;
  this.showCommentBox = false;

  this.toggleAllOff = function () {
    this.showNavBar = false;
    this.showCommentBox = false;
    this.showCollaboratorBox = false;
    this.showMessagesBox = false;
  };
});
